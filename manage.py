from datetime import datetime, timedelta
from flask.ext.script import Manager
from backend.app import initialize_app
import random, string
from backend.models import User
from config.settings import DefaultConfig

app = initialize_app(DefaultConfig)
manager = Manager(app)

# http://flask-script.readthedocs.org/en/latest/

def random_word(length):
    return ''.join(random.choice(string.lowercase) for i in range(length))

@manager.command
def populate(count=20):
    """Populate database with default data"""

    User.objects(tags='test').delete()

    for i in range(1, count):
        user = User()
        user.tags = ['test']
        user.username = random_word(random.randint(4,8))
        user.gender = random.choice([User.GENDER_FEMALE, User.GENDER_FEMALE])
        user.email = user.username + '@example.com'
        user.facebook_data = {
            'first_name': random_word(random.randint(4,8)).title(),
            'last_name': random_word(random.randint(4,8)).title(),
            'religion': random.choice(['Atheist', 'Christianity', 'Islam', 'Judaism', 'Buddhism']),
            'locale': random.choice(['en_US', 'fr_CA']),
            'likes': [
            ],
        }
        user.provider = 'facebook'
        user.birthday = datetime.now() - timedelta(days=random.randint(6000, 16000))
        user.facebook_id = random.randint(1000000,1000000000000)

        user.save(force_insert=True)


if __name__ == "__main__":
    manager.run()