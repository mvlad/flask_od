from distutils.core import setup

requires = [
]

setup(
    name='flask_od',
    version='1.0',
    packages=['backend', 'frontend', 'config'],
    url='',
    license='',
    author='Vlad Macari',
    author_email='vlad.macari@gmail.com',
    description='',
    install_requires=requires,
)
