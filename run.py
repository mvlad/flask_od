from backend.app import initialize_app
from config.settings import DefaultConfig

app = initialize_app(DefaultConfig)
app.run()