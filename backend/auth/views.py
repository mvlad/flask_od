from flask import g, Blueprint, flash, current_app, request, redirect
from flask.ext.login import login_required, login_user, logout_user

from backend.models import User

from social.actions import do_auth, do_complete
from social.apps.flask_app.utils import strategy
from social.exceptions import SocialAuthBaseException


auth = Blueprint('auth', __name__)

@login_required
@auth.route('/logout')
def handle_logout():
    logout_user()
    return redirect('/')


@auth.route('/login/<string:backend>/', methods=['GET', 'POST'])
@strategy('auth.complete')
def do(backend):
    return do_auth(g.strategy)


@auth.route('/complete/<string:backend>/', methods=['GET', 'POST'])
@strategy('auth.complete')
def complete(backend, *args, **kwargs):
    try:
        return do_complete(g.strategy, login=lambda strat, user: login_user(user, True),
            user=getattr(g, 'user', None), *args, **kwargs)
    except SocialAuthBaseException, e:
        flash('social_auth_error', 'error')
        return redirect('/#signin')


@auth.route('/dev_auth')
def dev():
    config = current_app.config
    facebook_id = request.args.get('fid', config.get('DEBUG_FACEBOOK_ID'))
    if config.get('DEBUG') and facebook_id:
        user = User.objects.get(facebook_id=facebook_id)
        if user:
            login_user(user, True)
        return redirect('/')
