from backend.models import User
from flask import g
from social.storage import base
from mongoengine import queryset
from time import strptime, mktime
from datetime import datetime
from social.backends.facebook import FacebookOAuth2


class SocialUser(base.UserMixin):
    user = None

    @classmethod
    def create_user(cls, username, email=None):
        if 'strategy' in g.__dict__:
            user = User()
            user.username = username
            user.email = email
            if g.strategy.backend_name == 'facebook':
                response = g.strategy.backend.response
                user.facebook_id = response.get('id')
                if response.get('birthday'):
                    timestamp = mktime(strptime(response.get('birthday'), '%m/%d/%Y'))
                    user.birthday = datetime.fromtimestamp(timestamp)
                user.gender = User.GENDER_MALE if response.get('gender')=='male' else User.GENDER_FEMALE
                if  response.get('likes'):
                    response['likes'] = [(l['category'], l['name'], l['id']) for l in response.get('likes').get('data')]
                user.facebook_data = response

            return user.save()

    def set_extra_data(self, extra_data=None):
        pass

    @classmethod
    def user_exists(cls, username):
        return False

    @classmethod
    def get_user(cls, pk):
        return User.objects.with_id(pk)

    @classmethod
    def changed(cls, user):
        user.save()

    @classmethod
    def username_max_length(cls):
        return User.username.max_length

    @classmethod
    def get_username(cls, user):
        return getattr(user, 'username', None)

    @classmethod
    def user_model(cls):
        return User


    @classmethod
    def get_social_auth(cls, provider, uid):
        try:
            user = cls.user_model().objects.get(**{provider+'_id': long(uid)})
            social = cls()
            social.user = user
            return social
        except queryset.DoesNotExist:
            return None

    @classmethod
    def create_social_auth(cls, user, uid, provider):
        return user

    @classmethod
    def allowed_to_disconnect(cls, user, backend_name, association_id=None):
        return False



class SocialMongoStorage(object):
    user = SocialUser
    nonce = None
    association = None

    @classmethod
    def is_integrity_error(cls, exception):
        return exception.__class__ is queryset.OperationError and\
               'E11000' in exception.message


class SFacebookOAuth2(FacebookOAuth2):
    response = None
    def user_data(self, access_token, *args, **kwargs):
        """Loads user data from service"""
        if self.response is None:
            params = self.setting('PROFILE_EXTRA_PARAMS', {})
            params['access_token'] = access_token
            self.response = self.get_json('https://graph.facebook.com/me',
                params=params)
        return self.response
