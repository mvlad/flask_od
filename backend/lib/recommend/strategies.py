from backend.models import User
from flask.ext.cache import Cache


class BaseStrategy(object):
    limit = 300

    def get_ids(self, preferences):
        """
        Returns recommended profile ids
        :param preferences: The `recommend.Preferences` object to define preferences
        :return:list of `ObjectId`
        """
        return []



class CacheStrategy(BaseStrategy):
    def get_ids(self, preferences):
        return []



class MongoDbStrategy(BaseStrategy):
    order = '-_id'

    def get_ids(self, preferences):
        criteria = {}
        if preferences.get_min_datetime():
            criteria['birthday__gte'] = preferences.get_min_datetime()
        if preferences.get_max_datetime():
            criteria['birthday__lte'] = preferences.get_max_datetime()
        if preferences.gender:
            criteria['gender'] = preferences.gender

        users = User.objects(**criteria).only('id').limit(self.limit).order_by(self.order)

        return [u.id for u in users]
