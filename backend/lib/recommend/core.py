from datetime import date, datetime, timedelta

class Preferences(object):
    def __init__(self, min_age=None, max_age=None, gender=None, interest=None, location=None, exclude_ids=None):
        # todo: add validation
        self.min_age = min_age
        self.max_age = max_age
        self.gender = gender
        self.interest = interest
        self.location = location
        self.exclude_ids = exclude_ids

    def get_min_datetime(self):
        if self.max_age:
            return datetime.now() - timedelta(days=self.max_age*365.24)
        return None

    def get_max_datetime(self):
        if self.min_age:
            return datetime.now() - timedelta(days=self.min_age*365.24)
        return None



class RecommendManager(object):
    """
    Recommendation Manager
    """
    def __init__(self, strategy):
        self.strategy = strategy

    def get_ids(self, preferences):
        return self.strategy.get_ids(preferences)

