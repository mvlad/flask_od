from flask import Blueprint, render_template, request, redirect
from flask.ext.login import current_user
from backend.models import ServerExports

main = Blueprint('main', __name__)

@main.route('/', defaults={'path': 'home'})
@main.route('/<path:path>')
def index(path):
    if request.path != '/':
        return redirect('/')

    server_exports = ServerExports()
    template_name = 'authenticated.html' if current_user.is_authenticated() else 'guest.html'

    return render_template(template_name, server_exports=server_exports.__dict__, path=path)
