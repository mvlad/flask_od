from flask.ext.login import LoginManager
login_manager = LoginManager()

from flask.ext.mongoengine import MongoEngine
db = MongoEngine()

from flask.ext.assets import Environment
assets = Environment()

