from flask import current_app, url_for, get_flashed_messages
from backend.extensions import db
from flask.ext import login
from mongoengine import StringField, IntField, ListField, LongField, DictField, DateTimeField


class User(db.Document, login.UserMixin):
    meta = {'allow_inheritance': False}

    STATUS_ACTIVE = 0
    STATUS_DELETED = 1
    STATUS_DISABLED = 2

    GENDER_MALE = 0
    GENDER_FEMALE = 1

    username = StringField(max_length=120)
    tags = ListField(StringField())
    status = IntField(required=True,default=STATUS_ACTIVE)
    email = StringField(max_length=120)
    birthday = DateTimeField()
    gender = IntField()

    facebook_id = LongField()
    facebook_data = DictField()

    vkontakte_id = LongField()
    vkontakte_data = DictField()

    user = None

    def __repr__(self):
        return '<User %r>' % (self.get_id())

    def set_extra_data(self, extra_data=None):
        pass


class ServerExports(object):
    authorize_urls = []
    social_apps = []
    flash_messages = None
    current_user = None
    development = None

    def __init__(self):
        self.development = current_app.config.get('DEBUG')
        self.authorize_urls = {
            'fb': url_for("auth.do", backend="facebook"),
            'vk': url_for("auth.do", backend="vk-oauth2"),
        }
        self.flash_messages = get_flashed_messages()
#        https://github.com/zeraholladay/Flask-Oauth2-Example/blob/master/app.py