from flask import Blueprint, jsonify, g, abort, request
from flask.views import MethodView
from backend.lib.recommend.core import Preferences, RecommendManager
from backend.lib.recommend.strategies import MongoDbStrategy
from backend.models import User
from utils import api_resource
import trafaret as t

api = Blueprint('api', __name__, None, None, None, '/api')

# https://gist.github.com/nimnull/2630160
@api_resource(api, 'profiles', {'id': None})
class ProfileResource(MethodView):
    def get(self, id=None):
        if id is None:
            rm = RecommendManager(MongoDbStrategy())
            preferences = Preferences(10, 100)

            ids = rm.get_ids(preferences)
            profiles = User.objects.in_bulk(ids)
            return jsonify(profiles=[{'id': str(p.id), 'name': p.username, 'gender': p.gender} for p in profiles.values()])
        else:
            return jsonify({'name': g.user.username})


    def put(self):
        """
        http://nimnull.blogspot.ru/2012/05/flask-methodview-restful-registration.html
        see also https://github.com/Deepwalker/trafaret
        :return:
        """
        user = g.user
        try:
            #  user.update(**self.validation.check(request.json))
            response, status = user.as_dict(), 202
        except t.DataError as e:
            response, status = e.as_dict(), 400

        return jsonify(response, status=status)
