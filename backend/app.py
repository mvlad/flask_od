from flask import Flask, g
from .main import main
from .api import api
from .auth import auth
from models import User
from .extensions import login_manager, db, assets
from flask.ext.login import current_user
import os

__all__ = ['initialize_app']


DEFAULT_BLUEPRINTS = (
    main,
    auth,
    api,
)


def initialize_app(config=None, app_name=None, blueprints=None):
    """Create application
    """
    if app_name is None:
        app_name = __name__
    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS

    app = Flask(app_name)
    configure_app(app, config)
    configure_blueprints(app, blueprints)
    configure_extensions(app)

    return app


def configure_app(app, config=None):
    app.config.from_object(config)
    app.config.from_pyfile('production.cfg', silent=True)


def configure_blueprints(app, blueprints):
    """Configure blueprints in views."""

    for blueprint in blueprints:
        app.register_blueprint(blueprint)



def configure_extensions(app):
    """
    https://flask-mongoengine.readthedocs.org/en/latest/
    """

    # flask-login
    login_manager.login_view = 'app.login'
    login_manager.refresh_view = 'app.login'

    #TODO: move this functions away

    @login_manager.user_loader
    def load_user(id):
        g.user = User.objects.only('facebook_id', 'username', 'tags').with_id(id)
        return g.user


    login_manager.init_app(app)

    assets.init_app(app)
    assets.from_yaml(os.path.join(app.root_path, '../config/assets.yaml'))

    db.init_app(app)