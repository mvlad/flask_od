App.module "Registration", (Registration, App, Backbone, Marionette, $, _) ->

    @startWithParent = false

    Registration.Controller = Marionette.Controller.extend
        signin: ->
            App.mainRegion.show(new Registration.SigninView({template: 'registration/signin'}))
        signup: ->
            App.mainRegion.show(new Registration.SigninView({template: 'registration/signup'}))

    Registration.Router = Marionette.AppRouter.extend
        appRoutes:
            'signin': 'signin'
            'signup': 'signup'


    Registration.SigninView = Backbone.Marionette.ItemView.extend
        events:
            'click .login-btn, .signup-btn': 'signin'

        onRender: () ->
            @$el.find('.global_error').toggleClass 'hidden',
            !_.contains(ServerExports.flash_messages, 'social_auth_error')

        signin: (e)->

            network = $(e.currentTarget).data('network')
            if network and network of ServerExports.authorize_urls
                window.location.href = ServerExports.authorize_urls[network]
            #                    + '?next=' + encodeURIComponent('/' + window.location.hash)
            false

    App.addInitializer (options) ->
        new Registration.Router({controller: new Registration.Controller})
