App.module "Browse", (Browse, App, Backbone, Marionette, $, _)->

    Browse.Controller = Marionette.Controller.extend
        initialize: (options) ->
            Browse._layout = new Browse.Layout()
            Browse._profiles = new Browse.ProfilesCollection()

        start: ->
            App.mainRegion.show(Browse._layout)

        showProfile: (id) ->
            profile = Browse._profiles.get(id)
            if profile
                Browse._layout.profile.show(new Browse.ProfileView({model: profile}))

    Browse.Router = Marionette.AppRouter.extend
        appRoutes:
            '': 'start'
            'profile/:id': 'showProfile'



    Browse.Profile = Backbone.Model.extend
        idAttribute: "id"
        defaults:
            id: "id"
            name: "name"

        initialize: ->
            console.log 'Profile'


    Browse.ProfilesCollection = Backbone.Collection.extend
        model: Browse.Profile
        url: '/api/profiles/'
        parse: (response) ->
            response.profiles






    App.addInitializer (options) ->
        controller = new Browse.Controller
        new Browse.Router({controller: controller})
        controller.start()