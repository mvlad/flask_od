App.module "Browse", (Browse, App, Backbone, Marionette, $, _)->
    Browse.Layout = Backbone.Marionette.Layout.extend
        template: 'browse/layout'

        regions:
            profile: "#browse-profile",
            ribbon: "#browse-ribbon"

        onShow: ->

            Browse._profiles.fetch()
            @ribbon.show(new Browse.RibbonView({
                'collection': Browse._profiles
            }))


    Browse.ProfileView = Backbone.Marionette.ItemView.extend
        template: 'browse/profile'


    Browse.RibbonProfileView = Backbone.Marionette.ItemView.extend
        template: 'browse/ribbon_item'
        tagName: 'li'


    Browse.RibbonEmptyView = Backbone.Marionette.ItemView.extend
        template: 'browse/ribbon_empty'
        tagName: 'li'


    Browse.RibbonView = Backbone.Marionette.CollectionView.extend
        template: 'browse/ribbon_main'
        tagName: "ul"
        itemView: Browse.RibbonProfileView
        emptyView: Browse.RibbonEmptyView