window.App = new Marionette.Application
    Collections: {}
    Models: {}
    Events: {}
    Views: {}


App.on 'initialize:after', ->
    if (Backbone.history)
        Backbone.history.start()

Backbone.Marionette.Renderer.render =(template, data) ->
    if !JST[template]
        throw "Template '" + template + "' not found!"
    JST[template](data)


App.addRegions
    mainRegion: "#container",
    navigationRegion: "#navbar"
    modalRegion: '#modal'
    sidebarRegion: '#sidebar'
###    Backbone.Marionette.Region.extend
        el: '#modal'
        onShow: (view) ->
            this.on("view:show", @showModal, this);
        showModal: (view) ->
            alert(3)###


#        constructor: ->
#            this.on("view:show", this.showModal, this);
#        showModal: (view) ->
#            view.on("close", @hideModal, this)
#            @$el.modal 'show'
#        hideModal: ->
#            @$el.modal 'hide'
