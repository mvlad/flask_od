Backbone.Marionette.ModalView = Marionette.ItemView.extend

#    template:
    content_template: null

    modal_events:
        'click .modal-backdrop': 'close'

    initialize: ->
        @events = _.extend(@events||{}, @modal_events)
        @on 'render', @insertContent
        @on 'show', @fixPosition


    insertContent: ->
        # TODO: add loader indicator
        html = Marionette.Renderer.render(@content_template)
        @$el.find('.modal-body').html(html)

    fixPosition: ->
        modal_body = this.$el.find('.modal')
        modal_body.css
            'margin-top': ($(window).height()-modal_body.height())/2
            'top': '0'
