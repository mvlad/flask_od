window.App = new Marionette.Application({
  Collections: {},
  Models: {},
  Events: {},
  Views: {}
});

App.on('initialize:after', function() {
  if (Backbone.history) {
    return Backbone.history.start();
  }
});

Backbone.Marionette.Renderer.render = function(template, data) {
  if (!JST[template]) {
    throw "Template '" + template + "' not found!";
  }
  return JST[template](data);
};

App.addRegions({
  mainRegion: "#container",
  navigationRegion: "#navbar",
  modalRegion: '#modal',
  sidebarRegion: '#sidebar'
});

/*    Backbone.Marionette.Region.extend
        el: '#modal'
        onShow: (view) ->
            this.on("view:show", @showModal, this);
        showModal: (view) ->
            alert(3)
*/


Backbone.Marionette.ModalView = Marionette.ItemView.extend({
  content_template: null,
  modal_events: {
    'click .modal-backdrop': 'close'
  },
  initialize: function() {
    this.events = _.extend(this.events || {}, this.modal_events);
    this.on('render', this.insertContent);
    return this.on('show', this.fixPosition);
  },
  insertContent: function() {
    var html;
    html = Marionette.Renderer.render(this.content_template);
    return this.$el.find('.modal-body').html(html);
  },
  fixPosition: function() {
    var modal_body;
    modal_body = this.$el.find('.modal');
    return modal_body.css({
      'margin-top': ($(window).height() - modal_body.height()) / 2,
      'top': '0'
    });
  }
});

App.module("Browse", function(Browse, App, Backbone, Marionette, $, _) {
  Browse.Controller = Marionette.Controller.extend({
    initialize: function(options) {
      Browse._layout = new Browse.Layout();
      return Browse._profiles = new Browse.ProfilesCollection();
    },
    start: function() {
      return App.mainRegion.show(Browse._layout);
    },
    showProfile: function(id) {
      var profile;
      profile = Browse._profiles.get(id);
      if (profile) {
        return Browse._layout.profile.show(new Browse.ProfileView({
          model: profile
        }));
      }
    }
  });
  Browse.Router = Marionette.AppRouter.extend({
    appRoutes: {
      '': 'start',
      'profile/:id': 'showProfile'
    }
  });
  Browse.Profile = Backbone.Model.extend({
    idAttribute: "id",
    defaults: {
      id: "id",
      name: "name"
    },
    initialize: function() {
      return console.log('Profile');
    }
  });
  Browse.ProfilesCollection = Backbone.Collection.extend({
    model: Browse.Profile,
    url: '/api/profiles/',
    parse: function(response) {
      return response.profiles;
    }
  });
  return App.addInitializer(function(options) {
    var controller;
    controller = new Browse.Controller;
    new Browse.Router({
      controller: controller
    });
    return controller.start();
  });
});

App.module("Registration", function(Registration, App, Backbone, Marionette, $, _) {
  this.startWithParent = false;
  Registration.Controller = Marionette.Controller.extend({
    signin: function() {
      return App.mainRegion.show(new Registration.SigninView({
        template: 'registration/signin'
      }));
    },
    signup: function() {
      return App.mainRegion.show(new Registration.SigninView({
        template: 'registration/signup'
      }));
    }
  });
  Registration.Router = Marionette.AppRouter.extend({
    appRoutes: {
      'signin': 'signin',
      'signup': 'signup'
    }
  });
  Registration.SigninView = Backbone.Marionette.ItemView.extend({
    events: {
      'click .login-btn, .signup-btn': 'signin'
    },
    onRender: function() {
      return this.$el.find('.global_error').toggleClass('hidden', !_.contains(ServerExports.flash_messages, 'social_auth_error'));
    },
    signin: function(e) {
      var network;
      network = $(e.currentTarget).data('network');
      if (network && network in ServerExports.authorize_urls) {
        window.location.href = ServerExports.authorize_urls[network];
      }
      return false;
    }
  });
  return App.addInitializer(function(options) {
    return new Registration.Router({
      controller: new Registration.Controller
    });
  });
});

App.module("Browse", function(Browse, App, Backbone, Marionette, $, _) {
  Browse.Layout = Backbone.Marionette.Layout.extend({
    template: 'browse/layout',
    regions: {
      profile: "#browse-profile",
      ribbon: "#browse-ribbon"
    },
    onShow: function() {
      Browse._profiles.fetch();
      return this.ribbon.show(new Browse.RibbonView({
        'collection': Browse._profiles
      }));
    }
  });
  Browse.ProfileView = Backbone.Marionette.ItemView.extend({
    template: 'browse/profile'
  });
  Browse.RibbonProfileView = Backbone.Marionette.ItemView.extend({
    template: 'browse/ribbon_item',
    tagName: 'li'
  });
  Browse.RibbonEmptyView = Backbone.Marionette.ItemView.extend({
    template: 'browse/ribbon_empty',
    tagName: 'li'
  });
  return Browse.RibbonView = Backbone.Marionette.CollectionView.extend({
    template: 'browse/ribbon_main',
    tagName: "ul",
    itemView: Browse.RibbonProfileView,
    emptyView: Browse.RibbonEmptyView
  });
});

App.module("Main", function(Main, App, Backbone, Marionette, $, _) {
  return App.ProfileView = Backbone.Marionette.ItemView.extend({
    initialize: function() {}
  });
});
