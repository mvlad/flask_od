from fabric.api import *

# the user to use for the remote commands
env.user = 'vlad'
# the servers where the commands are executed
env.hosts = ['localhost', 's.dev']

def pack():
    # create a new source distribution as tarball
    local('python setup.py sdist --formats=gztar', capture=False)

def deploy():
    # figure out the release name and version
    dist = local('python setup.py --fullname', capture=True).strip()
    # upload the source tarball to the temporary folder on the server
    put('dist/%s.tar.gz' % dist, '/tmp/flask_od.tar.gz')
    # create a place where we can unzip the tarball, then enter
    # that directory and unzip it
    run('mkdir /tmp/yourapplication')
    with cd('/tmp/yourapplication'):
        run('tar xzf /tmp/yourapplication.tar.gz')
        # now setup the package with our virtual environment's
        # python interpreter
        run('/var/www/flask_od/env/bin/python setup.py install')
        # now that all is set up, delete the folder again
    run('rm -rf /tmp/flask_od /tmp/yourapplication.tar.gz')
    run('apt-get install npm')
    # TODO: install mongodb
    run('npm install -g coffee-script')
    # and finally touch the .wsgi file so that mod_wsgi triggers
    # a reload of the application
    run('touch /var/www/flask_od.wsgi')