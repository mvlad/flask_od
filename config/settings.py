import os

class BaseConfig(object):

    PROJECT = "S"

    DEBUG = False
    TESTING = False

    ADMINS = frozenset(['xvladislav@gmail.com'])

    # http://flask.pocoo.org/docs/quickstart/#sessions
    SECRET_KEY = 'vjsliav9asd8yvp9ds8yv9s8dpvhysdhv'

    # https://github.com/omab/python-social-auth/blob/master/docs/configuration/settings.rst
    SOCIAL_AUTH_USER_MODEL = 'backend.models.User'
    SOCIAL_AUTH_STORAGE = 'backend.auth.models.SocialMongoStorage'
    SOCIAL_AUTH_AUTHENTICATION_BACKENDS = (
#        'social.backends.facebook.FacebookOAuth2',
        'backend.auth.models.SFacebookOAuth2',
    )
    SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email', 'first_name', 'last_name', 'fullname']
    SOCIAL_AUTH_PROFILE_EXTRA_PARAMS = {
        'fields': 'id,name,email,gender,first_name,last_name,username,birthday,likes,work,languages,location,locale,religion,political,quotes,timezone'
    }

#    LOG_FOLDER = os.path.join(INSTANCE_FOLDER_PATH, 'logs')
#    make_dir(LOG_FOLDER)

    # Fild upload, should override in production.
    # Limited the maximum allowed payload to 16 megabytes.
    # http://flask.pocoo.org/docs/patterns/fileuploads/#improving-uploads
#    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
#    UPLOAD_FOLDER = os.path.join(INSTANCE_FOLDER_PATH, 'uploads')
#    make_dir(UPLOAD_FOLDER)


class DefaultConfig(BaseConfig):
    DEBUG = True
    ASSETS_DEBUG = DEBUG
    DEBUG_FACEBOOK_ID = long(1468464913)
    DEBUG_TB_PANELS = [
        'flask_debugtoolbar.panels.timer.TimerDebugPanel',
        'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
        'flask_debugtoolbar.panels.logger.LoggingPanel',
        'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',

        'flask.ext.mongoengine.panels.MongoDebugPanel'
    ]

    ADMINS = frozenset(['xvladislav@gmail.com'])
    SECRET_KEY = 'SecretKeyForSessionSigning'

    #SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'app.db')
    #DATABASE_CONNECT_OPTIONS = {}
    #
    #THREADS_PER_PAGE = 2

    CSRF_ENABLED=True
    CSRF_SESSION_KEY="somethingimpossibletoguess"

    SOCIAL_AUTH_FACEBOOK_KEY='207378372725669'
    SOCIAL_AUTH_FACEBOOK_SECRET='460d3b3ecb0d6d282110d16f8ea6892d'
    SOCIAL_AUTH_FACEBOOK_SCOPE=['email','user_photos','user_work_history','user_activities','user_likes','user_interests','user_birthday']

    MONGODB_DB = 's'


class TestConfig(BaseConfig):
    TESTING = True
    CSRF_ENABLED = False

